// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Ballmasters_BakerHUD.generated.h"

UCLASS()
class ABallmasters_BakerHUD : public AHUD
{
	GENERATED_BODY()

public:
	ABallmasters_BakerHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};


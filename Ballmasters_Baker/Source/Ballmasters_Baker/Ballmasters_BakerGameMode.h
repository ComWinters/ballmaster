// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Ballmasters_BakerGameMode.generated.h"

UCLASS(minimalapi)
class ABallmasters_BakerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABallmasters_BakerGameMode();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int Score;
};




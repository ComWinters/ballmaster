// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Ballmasters_BakerGameMode.h"
#include "Ballmasters_BakerHUD.h"
#include "Ballmasters_BakerCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABallmasters_BakerGameMode::ABallmasters_BakerGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ABallmasters_BakerHUD::StaticClass();
}

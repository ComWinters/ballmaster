// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef BALLMASTERS_BAKER_Ballmasters_BakerProjectile_generated_h
#error "Ballmasters_BakerProjectile.generated.h already included, missing '#pragma once' in Ballmasters_BakerProjectile.h"
#endif
#define BALLMASTERS_BAKER_Ballmasters_BakerProjectile_generated_h

#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABallmasters_BakerProjectile(); \
	friend BALLMASTERS_BAKER_API class UClass* Z_Construct_UClass_ABallmasters_BakerProjectile(); \
public: \
	DECLARE_CLASS(ABallmasters_BakerProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Ballmasters_Baker"), NO_API) \
	DECLARE_SERIALIZER(ABallmasters_BakerProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABallmasters_BakerProjectile(); \
	friend BALLMASTERS_BAKER_API class UClass* Z_Construct_UClass_ABallmasters_BakerProjectile(); \
public: \
	DECLARE_CLASS(ABallmasters_BakerProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Ballmasters_Baker"), NO_API) \
	DECLARE_SERIALIZER(ABallmasters_BakerProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABallmasters_BakerProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABallmasters_BakerProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABallmasters_BakerProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABallmasters_BakerProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABallmasters_BakerProjectile(ABallmasters_BakerProjectile&&); \
	NO_API ABallmasters_BakerProjectile(const ABallmasters_BakerProjectile&); \
public:


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABallmasters_BakerProjectile(ABallmasters_BakerProjectile&&); \
	NO_API ABallmasters_BakerProjectile(const ABallmasters_BakerProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABallmasters_BakerProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABallmasters_BakerProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABallmasters_BakerProjectile)


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ABallmasters_BakerProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ABallmasters_BakerProjectile, ProjectileMovement); }


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_9_PROLOG
#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_RPC_WRAPPERS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_INCLASS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_INCLASS_NO_PURE_DECLS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

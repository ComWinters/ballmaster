// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Ballmasters_BakerHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBallmasters_BakerHUD() {}
// Cross Module References
	BALLMASTERS_BAKER_API UClass* Z_Construct_UClass_ABallmasters_BakerHUD_NoRegister();
	BALLMASTERS_BAKER_API UClass* Z_Construct_UClass_ABallmasters_BakerHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Ballmasters_Baker();
// End Cross Module References
	void ABallmasters_BakerHUD::StaticRegisterNativesABallmasters_BakerHUD()
	{
	}
	UClass* Z_Construct_UClass_ABallmasters_BakerHUD_NoRegister()
	{
		return ABallmasters_BakerHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_ABallmasters_BakerHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AHUD,
				(UObject* (*)())Z_Construct_UPackage__Script_Ballmasters_Baker,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Rendering Actor Input Replication" },
				{ "IncludePath", "Ballmasters_BakerHUD.h" },
				{ "ModuleRelativePath", "Ballmasters_BakerHUD.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ABallmasters_BakerHUD>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ABallmasters_BakerHUD::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x0080028Cu,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABallmasters_BakerHUD, 2882210142);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABallmasters_BakerHUD(Z_Construct_UClass_ABallmasters_BakerHUD, &ABallmasters_BakerHUD::StaticClass, TEXT("/Script/Ballmasters_Baker"), TEXT("ABallmasters_BakerHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABallmasters_BakerHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

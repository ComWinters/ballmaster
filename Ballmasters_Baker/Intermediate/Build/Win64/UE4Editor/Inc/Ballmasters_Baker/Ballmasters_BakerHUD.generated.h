// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BALLMASTERS_BAKER_Ballmasters_BakerHUD_generated_h
#error "Ballmasters_BakerHUD.generated.h already included, missing '#pragma once' in Ballmasters_BakerHUD.h"
#endif
#define BALLMASTERS_BAKER_Ballmasters_BakerHUD_generated_h

#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_RPC_WRAPPERS
#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABallmasters_BakerHUD(); \
	friend BALLMASTERS_BAKER_API class UClass* Z_Construct_UClass_ABallmasters_BakerHUD(); \
public: \
	DECLARE_CLASS(ABallmasters_BakerHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Ballmasters_Baker"), NO_API) \
	DECLARE_SERIALIZER(ABallmasters_BakerHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABallmasters_BakerHUD(); \
	friend BALLMASTERS_BAKER_API class UClass* Z_Construct_UClass_ABallmasters_BakerHUD(); \
public: \
	DECLARE_CLASS(ABallmasters_BakerHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Ballmasters_Baker"), NO_API) \
	DECLARE_SERIALIZER(ABallmasters_BakerHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABallmasters_BakerHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABallmasters_BakerHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABallmasters_BakerHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABallmasters_BakerHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABallmasters_BakerHUD(ABallmasters_BakerHUD&&); \
	NO_API ABallmasters_BakerHUD(const ABallmasters_BakerHUD&); \
public:


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABallmasters_BakerHUD(ABallmasters_BakerHUD&&); \
	NO_API ABallmasters_BakerHUD(const ABallmasters_BakerHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABallmasters_BakerHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABallmasters_BakerHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABallmasters_BakerHUD)


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_9_PROLOG
#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_RPC_WRAPPERS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_INCLASS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_INCLASS_NO_PURE_DECLS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UPrimitiveComponent;
struct FVector;
struct FHitResult;
#ifdef BALLMASTERS_BAKER_Ballmasters_BakerCharacter_generated_h
#error "Ballmasters_BakerCharacter.generated.h already included, missing '#pragma once' in Ballmasters_BakerCharacter.h"
#endif
#define BALLMASTERS_BAKER_Ballmasters_BakerCharacter_generated_h

#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execdefeated) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_delta); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->defeated(Z_Param_delta); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execdefeated) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_delta); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->defeated(Z_Param_delta); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABallmasters_BakerCharacter(); \
	friend BALLMASTERS_BAKER_API class UClass* Z_Construct_UClass_ABallmasters_BakerCharacter(); \
public: \
	DECLARE_CLASS(ABallmasters_BakerCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Ballmasters_Baker"), NO_API) \
	DECLARE_SERIALIZER(ABallmasters_BakerCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesABallmasters_BakerCharacter(); \
	friend BALLMASTERS_BAKER_API class UClass* Z_Construct_UClass_ABallmasters_BakerCharacter(); \
public: \
	DECLARE_CLASS(ABallmasters_BakerCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Ballmasters_Baker"), NO_API) \
	DECLARE_SERIALIZER(ABallmasters_BakerCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABallmasters_BakerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABallmasters_BakerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABallmasters_BakerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABallmasters_BakerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABallmasters_BakerCharacter(ABallmasters_BakerCharacter&&); \
	NO_API ABallmasters_BakerCharacter(const ABallmasters_BakerCharacter&); \
public:


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABallmasters_BakerCharacter(ABallmasters_BakerCharacter&&); \
	NO_API ABallmasters_BakerCharacter(const ABallmasters_BakerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABallmasters_BakerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABallmasters_BakerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABallmasters_BakerCharacter)


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ABallmasters_BakerCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ABallmasters_BakerCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ABallmasters_BakerCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ABallmasters_BakerCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ABallmasters_BakerCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ABallmasters_BakerCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ABallmasters_BakerCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ABallmasters_BakerCharacter, L_MotionController); }


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_11_PROLOG
#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_RPC_WRAPPERS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_INCLASS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Ballmasters_Baker_Source_Ballmasters_Baker_Ballmasters_BakerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
